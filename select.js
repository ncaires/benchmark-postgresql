const { Client } = require("pg");

const client = new Client({
  user: "postgres",
  host: "localhost",
  database: "teste",
  password: "123456",
  port: 5432
});

start();

function start() {
  client
    .connect()
    .then(() => {
      selecProgramas()
        .then(() => {
          client.end();
        })
        .catch(err => {
          console.log(err);
        });
    })
    .catch(err => {
      console.log(err);
    });
}

async function selecProgramas() {
  var text = "SELECT * from programa";
  try {
    const res = await client.query(text);
    var programas = res.rows;
    for (let i = 0; i < programas.length; i++) {
      programas[i].espelhos = await selectEspelhos(programas[i].programa_id);
    }
    console.log(programas[0].espelhos);
    return Promise.resolve(true);
  } catch (err) {
    console.log(err.stack);
  }
}

async function selectEspelhos(programa_id) {
  var text = "SELECT * from espelho where programa_id = $1";
  var values = [programa_id];

  var espelhos = [];

  try {
    const res = await client.query(text, values);
    espelhos = res.rows;
    for (let i = 0; i < espelhos.length; i++) {
      espelhos[i].blocos = await selectBloco(espelhos[i].espelho_id);
    }
    return espelhos;
  } catch (err) {
    console.log(err.stack);
  }
}

async function selectBloco(espelho_id) {
  var text = "SELECT * from bloco where espelho_id = $1";
  var values = [espelho_id];

  var blocos = [];

  try {
    const res = await client.query(text, values);
    blocos = res.rows;
    for (let i = 0; i < blocos.length; i++) {
      blocos[i].laudas = await selectLauda(blocos[i].bloco_id);
    }
    return blocos;
  } catch (err) {
    console.log(err.stack);
  }
}

async function selectLauda(bloco_id) {
  var text = "SELECT * from lauda where bloco_id = $1";
  var values = [bloco_id];

  var laudas = [];

  try {
    const res = await client.query(text, values);
    laudas = res.rows;
    for (let i = 0; i < laudas.length; i++) {
      laudas[i].textos = await selectTexto(laudas[i].lauda_id);
    }
    return laudas;
  } catch (err) {
    console.log(err.stack);
  }
}

async function selectTexto(lauda_id) {
  var text = "SELECT * from texto where lauda_id = $1";
  var values = [lauda_id];

  try {
    const res = await client.query(text, values);
    return res.rows;
  } catch (err) {
    console.log(err.stack);
  }
}
