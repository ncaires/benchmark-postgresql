const { Client } = require("pg");
const texto = require("./texto.json").texto;

const qtdEspelho = 50;
const qtdBlocos = 5;
const qtdLaudas = 20;
const qtdTextos = 5;

const client = new Client({
  user: "postgres",
  host: "localhost",
  database: "teste",
  password: "123456",
  port: 5432
});

start();

function start() {
  client
    .connect()
    .then(() => {
      insert()
        .then(() => {
          client.end();
        })
        .catch(err => {
          console.log(err);
        });
    })
    .catch(err => {
      console.log(err);
    });
}

async function insert() {
  var text = "INSERT INTO programa(programaname) VALUES($1) RETURNING *";
  var values = ["teste"];
  try {
    var res = await client.query(text, values);
    await insertEspelho(res.rows[0].programa_id);
    return Promise.resolve(true);
  } catch (err) {
    console.log(err.stack);
  }
}

async function insertEspelho(programa_id) {
  var text =
    "INSERT INTO espelho(dataespelho, programa_id) VALUES($1, $2) RETURNING *";

  for (var i = 0; i < qtdEspelho; i++) {
    var values = [new Date(), programa_id];
    try {
      var res = await client.query(text, values);
      await insertBloco(res.rows[0].espelho_id);
    } catch (err) {
      console.log(err.stack);
    }
  }
}

async function insertBloco(espelho_id) {
  var text =
    "INSERT INTO bloco(duracao, posicao, espelho_id) VALUES($1, $2, $3) RETURNING *";
  for (var i = 0; i < qtdBlocos; i++) {
    var values = [new Date(), i, espelho_id];
    try {
      var res = await client.query(text, values);
      await insertLauda(res.rows[0].bloco_id);
    } catch (err) {
      console.log(err.stack);
    }
  }
}

async function insertLauda(bloco_id) {
  var text =
    "INSERT INTO lauda(titulo, duracao, posicao, bloco_id, reporter_id, editor_id) VALUES($1, $2, $3, $4, $5, $6) RETURNING *";
  var reporterid = await insertReporter();
  var editorid = await insertEditor();
  for (var i = 0; i < qtdLaudas; i++) {
    var values = [i, new Date(), i, bloco_id, reporterid, editorid];
    try {
      var res = await client.query(text, values);
      await insertTexto(res.rows[0].lauda_id);
    } catch (err) {
      console.log(err.stack);
    }
  }
}

async function insertTexto(lauda_id) {
  var text =
    "INSERT INTO texto(texto, posicao, lauda_id) VALUES($1, $2, $3) RETURNING *";
  for (var i = 0; i < qtdTextos; i++) {
    var values = [texto, i, lauda_id];

    try {
      await client.query(text, values);
    } catch (err) {
      console.log(err.stack);
    }
  }
}

async function insertReporter() {
  var text = "INSERT INTO reporter(reportername) VALUES($1) RETURNING *";
  var values = ["Nome do reporter"];
  try {
    const res = await client.query(text, values);
    return res.rows[0].reporter_id;
  } catch (err) {
    console.log(err.stack);
  }
}

async function insertEditor() {
  var text = "INSERT INTO editor(editorname) VALUES($1) RETURNING *";
  var values = ["Nome do editor"];
  try {
    const res = await client.query(text, values);
    return res.rows[0].editor_id;
  } catch (err) {
    console.log(err.stack);
  }
}
